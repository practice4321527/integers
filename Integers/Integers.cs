[assembly: CLSCompliant(false)]

namespace Literals
{
    public static class Integers
    {
        public static int ReturnInteger11()
        {
            // TODO #1-1. Return "0" literal.
            throw new NotImplementedException();
        }

        public static int ReturnInteger12()
        {
            // TODO #1-2. Return "1" literal.
            throw new NotImplementedException();
        }

        public static int ReturnInteger13()
        {
            // TODO #1-3. Return "-1" literal.
            throw new NotImplementedException();
        }

        public static int ReturnInteger14()
        {
            // TODO #1-4. Return "2,147,483,647" literal.
            throw new NotImplementedException();
        }

        public static int ReturnInteger15()
        {
            // TODO #1-5. Return "-2,147,483,648" literal.
            throw new NotImplementedException();
        }

        public static uint ReturnInteger16()
        {
            // TODO #1-6. Return "1" literal.
            throw new NotImplementedException();
        }

        public static uint ReturnInteger17()
        {
            // TODO #1-7. Return "32,767" literal.
            throw new NotImplementedException();
        }

        public static uint ReturnInteger18()
        {
            // TODO #1-8. Return "2,147,483,647" literal.
            throw new NotImplementedException();
        }
    }
}
